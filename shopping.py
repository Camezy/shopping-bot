from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import Select
import time

service = Service(executable_path="/Users/cam/Desktop/chromedriver-mac-arm64 2/chromedriver")

driver = webdriver.Chrome(service=service)

driver.get("https://telfar.net/collections/circle-bags/products/round-telfar-circle-bag-black")


WebDriverWait(driver, 5).until(
    EC.presence_of_element_located((By.XPATH, '//*[@id="product-form"]/div/button'))
)


input_element = driver.find_element(By.XPATH, '//*[@id="product-form"]/div/button')
input_element.click()

# When using XPATH, you can ONLY use Single Quotes

WebDriverWait(driver, 8).until(
    EC.presence_of_element_located((By.XPATH, '//*[@id="MainContent"]/div[6]/div/form/footer/div/div[2]/p[4]/button'))
)

check_out = driver.find_element(By.XPATH, '//*[@id="MainContent"]/div[6]/div/form/footer/div/div[2]/p[4]/button')
check_out.click()


email = driver.find_element(By.XPATH, '//*[@id="checkout_email"]')
email.send_keys("Testing@gmail.com")

first_name = driver.find_element(By.ID, "checkout_shipping_address_first_name")
first_name.send_keys("Cameron")


last_name = driver.find_element(By.ID, "checkout_shipping_address_last_name")
last_name.send_keys("R")

address = driver.find_element(By.ID, "checkout_shipping_address_address1")
address.send_keys("121 Maple Street")

city = driver.find_element(By.ID, "checkout_shipping_address_city")
city.send_keys("Austin")


state = driver.find_element(By.XPATH, '//*[@id="checkout_shipping_address_province"]')
sstate = Select(state)
sstate.select_by_visible_text("Texas")

zipcode = driver.find_element(By.ID, "checkout_shipping_address_zip")
zipcode.send_keys("12345")

phone = driver.find_element(By.ID, "checkout_shipping_address_phone")
phone.send_keys("2019388415")


continue_shopping = driver.find_element(By.ID, "continue_button")
continue_shopping.click()

time.sleep(15)
